//hi
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include "FS.h"
#include <SPI.h>
#include <SD.h>
#include <MyRealTimeClock.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#include <HTTPUpdate.h>

WiFiMulti WiFiMulti;
WiFiClient client;
LiquidCrystal_I2C lcd(0x27, 16, 2);

MyRealTimeClock myRTC(14, 12, 13); // Pin assigned 
int loop_count_for_lcd=0;                        
File myFile;
String sent_time;
String pre_sent_time;
int min_time=23;
const char* host = "www.google.com";
const int chipSelect = 4;
int sd_card_state = 0;
int post_data_state = 0;
int sd_card_empty = 1;
int punches_exhausted_in_sd_card=0;
int i = 0;
int j = 0;
int count_of_punches_in_sd_card = 0;
String fingerprint_records;
String dataFromArduino;
char dataFromArduino_;
int init_=1;
int count_for_time_update=0;
int flag_for_time_update=0;

String dataFromAll;
int string_data_from_arduino[3000];
char sd_card_data[3000];
String response;
HTTPClient http;
int read_data;
int pre_read_data=3000;
int count_for_connection = 0;
int internet_connected = 0;
const long utcOffsetInSeconds =  19800;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

void setup() {
  WiFi.begin("OSWAL TOWER", "oswal@321");
//  WiFi.begin("Realme", "0987654321");
//   WiFi.begin("JioFi3_075187", "15x5y75vbc");  
Serial.begin(9600);                                
   lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("WELCOME   ");
   lcd.setCursor(0,1);
  lcd.print("PLEASE WAIT...");
  lcd.print("            ");
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33,1);
  pinMode(GPIO_NUM_25,OUTPUT);
  digitalWrite(GPIO_NUM_25,LOW);
  delay(3000);
//  WiFi.begin("OSWAL TOWER", "oswal@321");
// WiFi.begin("JioFi3_075187", "15x5y75vbc");
 timeClient.begin();
  printLocalTime();

}

void loop() {
  myRTC.updateTime();
 
lcd_print();
if(init_==1){
   lcd.setCursor(0,1);
  lcd.print("READY");
  lcd.print("            ");
  init_=0;
}
if (WiFi.status() != WL_CONNECTED){
  count_for_connection=count_for_connection+1;
//     Serial.println(count_for_connection);
  }

while(count_for_connection==2000){
  count_for_connection=0;
//   WiFi.begin("Realme", "0987654321");
//WiFi.begin("JioFi3_075187", "15x5y75vbc");
  WiFi.begin("OSWAL TOWER", "oswal@321");
  }

  loop_count_for_lcd = loop_count_for_lcd+1;
  
   
  if ( Serial.available()>0) {
    
    count_for_connection=0;
    internet_connected = 0;
    loop_count_for_lcd=0;
    fingerData();
   }
   else{
    if (loop_count_for_lcd==200){
      lcd.setCursor(0,1);
  lcd.print("                      ");
    }
     if(myRTC.hours==13){
 count_for_time_update=count_for_time_update+1;
  }
  if(count_for_time_update==2000){
    printLocalTime();
    delay(2000);
     }
  if(myRTC.hours==14){
    count_for_time_update=0;
    flag_for_time_update=0;
  }
    if(internet_connected==5000){
      internet_connected = 0;
      if (client.connect(host, 80)){
        count_for_connection=0;  
     if((myRTC.hours)==min_time){
           readFile(SD, "/dailyData.txt");
          if(String(sd_card_data[j])=="?"){
            count_of_punches_in_sd_card+=1;
            if(count_of_punches_in_sd_card==10){
              punches_exhausted_in_sd_card=1;
              if(sd_card_state == 0){
                post_data_state=1;
                postData();
                count_of_punches_in_sd_card=0;    
              }
            }          
          }
        if(sd_card_state == 0){
          post_data_state=1;
          punches_exhausted_in_sd_card=0;
          postData();
          count_of_punches_in_sd_card=0;
          if(response=="done"){
            deleteData();
            sd_card_empty = 0;
          }
          sd_card_state=2;
        }
     }
     else{
         readFile(SD, "/sensor.txt");
          if(String(sd_card_data[j])=="?"){
            count_of_punches_in_sd_card+=1;
            if(count_of_punches_in_sd_card==10){
              punches_exhausted_in_sd_card=1;
              if(sd_card_state == 1){
                post_data_state=1;
                postData();
                count_of_punches_in_sd_card=0;    
              }
            }          
          }
        if(sd_card_state == 1){
          post_data_state=1;
          punches_exhausted_in_sd_card=0;
          postData();
          count_of_punches_in_sd_card=0;
          if(response=="done"){
            deleteData();
            sd_card_empty = 0;
          }
          sd_card_state=0;
        }
      }
    }
    else{
      internet_connected=internet_connected+1;
    }
    }
    else{
      internet_connected=internet_connected+1;
      Serial.println( internet_connected);
    }
  }
}
 

void fingerData(){
  while ( Serial.available()>0){
    dataFromArduino="";
    while ( Serial.available()>0){
     dataFromArduino_=Serial.read();
     dataFromArduino=dataFromArduino+String(dataFromArduino_);
    }
    int dataFromArduino_1=dataFromArduino.toInt();
    if(dataFromArduino_1<=2000){
    lcd.setCursor(0,0);
  lcd.print("WELCOME   ");
    sd_card_empty =1;
    sd_card_state = 1;
    sdCard();
  }
  else{
     if (dataFromArduino_1==9000){
    lcd.setCursor(0,1);
  lcd.print("Remove finger");
  lcd.print("          ");
  }

  if (dataFromArduino_1==9001){
      lcd.setCursor(0,0);
  lcd.print("REG MODE");
  lcd.print("          ");
    lcd.setCursor(0,1);
  lcd.print("waiting");
  lcd.print("          ");
  }
  if (dataFromArduino_1==9002){
    lcd.setCursor(0,0);
  lcd.print("REG MODE");
  lcd.print("          ");
    lcd.setCursor(0,1);
  lcd.print("PLACE AGAIN");
  lcd.print("          ");
  } 
  if (dataFromArduino_1==9003){
    lcd.setCursor(0,1);
  lcd.print("try again");
  lcd.print("          ");
  }   
}
  }
}

void postData(){
  fingerprint_records = dataFromAll;
   Serial.println(dataFromAll);
  if(post_data_state==1){
    http.begin("http://waytohr.herokuapp.com/transaction/fingerprint_records_entry?box_id=2&fingerprint_records=" +fingerprint_records);   //Specify request destination
    http.addHeader("Content-Type", "text/plain");  //Specify content-type header
    int httpCode = http.POST("fingerprint_records");   //Send the request
    String payload = http.getString();   //Get the response payload
    response = payload;
    //Serial.write(payload);
    Serial.println(response);
    dataFromAll="";
    post_data_state=0;
    if(punches_exhausted_in_sd_card==0){
      sd_card_state=0;
    }
  }
  http.end();
}

void sdCard(){
 if(!SD.begin()){
        return;
    }
    String name1=String(myRTC.year)+"-"+String( myRTC.month)+"-"+String(myRTC.dayofmonth)+"/"+String(myRTC.hours)+":"+String(myRTC.minutes)+":"+String(myRTC.seconds)+"/"+dataFromArduino+"?";
   String name2=String(myRTC.year)+"-"+String( myRTC.month)+"-"+String(myRTC.dayofmonth)+"/"+String(myRTC.hours)+":"+String(myRTC.minutes)+":"+String(myRTC.seconds)+"/"+dataFromArduino+"?";
 String name3=String(myRTC.year)+"-"+String( myRTC.month)+"-"+String(myRTC.dayofmonth)+"/"+String(myRTC.hours)+":"+String(myRTC.minutes)+":"+String(myRTC.seconds)+"/"+dataFromArduino+"?";
 
    appendFile(SD, "/sensor.txt",name1);
    appendFile(SD, "/dailyData.txt",name2);
    appendFile(SD, "/allData.txt",name3);
    digitalWrite(GPIO_NUM_25,HIGH);
  lcd.setCursor(0,1);
  lcd.print("THANK YOU  ");
  lcd.setCursor(11,1);
   lcd.print(dataFromArduino);
    lcd.print("    ");
   Serial.println( name1);
   delay(1000);
   digitalWrite(GPIO_NUM_25,LOW);
   
  
}

void deleteData(){
if((myRTC.hours)==min_time){
   deleteFile(SD, "/dailyData.txt");
}
else{
  deleteFile(SD, "/sensor.txt");
}
Serial.println("delete");
}

void printLocalTime()
{
  
  
  if (client.connect(host, 80)){
    timeClient.update();
String Msg =timeClient.getFormattedDate();
 int years=(Msg.substring(0, 4)).toInt();
 int months=(Msg.substring(5, 7)).toInt();
 int days =(Msg.substring(8, 10)).toInt();
  int hours = timeClient.getHours();
  int minutes = timeClient.getMinutes();
  int seconds = timeClient.getSeconds();
  if(hours==5){
  }
  else{
   myRTC.setDS1302Time(seconds, minutes , hours, 6, days, months, years);
  }
  }
}

void lcd_print(){
  lcd.setCursor(10,0);
  lcd.print(" ");
  lcd.print(myRTC.hours);
//  Serial.print(myRTC.hours);
  lcd.print(":");
//  Serial.print(":");
  lcd.print(myRTC.minutes);
//  Serial.println(myRTC.minutes);
   lcd.print("  ");
}

void readFile(fs::FS &fs, String path){
    File file = fs.open(path);
    if(!file){
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
         sd_card_data[j]=file.read();
         dataFromAll+=String(sd_card_data[j]);
    }
    file.close();
}

void writeFile(fs::FS &fs, String path, String message){
    File file = fs.open(path, FILE_WRITE);
    if(!file){
        return;
    }
    if(file.print(message)){
    } else {
    }
    file.close();
}
void deleteFile(fs::FS &fs, String path){
    if(fs.remove(path)){
    } else {
    }
}
void appendFile(fs::FS &fs, String path, String message){
    File file = fs.open(path, FILE_APPEND);
    if(!file){
        return;
    }
    if(file.print(message)){
    } else {
    }
    file.close();
}
